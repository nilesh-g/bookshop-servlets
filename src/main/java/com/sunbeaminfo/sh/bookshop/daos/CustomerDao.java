package com.sunbeaminfo.sh.bookshop.daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sunbeaminfo.sh.bookshop.entities.Book;
import com.sunbeaminfo.sh.bookshop.entities.Customer;
import com.sunbeaminfo.sh.bookshop.utils.DbUtil;

public class CustomerDao  implements AutoCloseable {
	private Connection con;
	public void open() throws Exception {
		con = DriverManager.getConnection(DbUtil.DB_URL, DbUtil.DB_USER, DbUtil.DB_PASS);
	}
	@Override
	public void close() {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
		}
	}
	public Customer getCustomer(String email) throws Exception {
		Customer c = null;
		String sql = "SELECT * FROM CUSTOMERS WHERE email=?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, email);
			try(ResultSet rs = stmt.executeQuery()) {
				if(rs.next()) {
					c = new Customer();
					c.setId( rs.getInt("id") );
					c.setName( rs.getString("name") );
					c.setEmail( rs.getString("email") );
					c.setPassword( rs.getString("password") );
					c.setMobile( rs.getString("mobile") );
					c.setAddress( rs.getString("address") );
				}
			}
		}
		return c;
	}
}
