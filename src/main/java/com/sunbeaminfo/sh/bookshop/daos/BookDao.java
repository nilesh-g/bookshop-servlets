package com.sunbeaminfo.sh.bookshop.daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLType;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.sunbeaminfo.sh.bookshop.entities.Book;
import com.sunbeaminfo.sh.bookshop.utils.DbUtil;

public class BookDao implements AutoCloseable {
	private Connection con;
	public void open() throws Exception {
		con = DriverManager.getConnection(DbUtil.DB_URL, DbUtil.DB_USER, DbUtil.DB_PASS);
	}
	@Override
	public void close() {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
		}
	}
	// get books of given subject
	public List<Book> getBooks(String subject) throws Exception {
		List<Book> list = new ArrayList<Book>();
		String sql = "SELECT * FROM BOOKS WHERE subject=?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, subject);
			try(ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					Book b = new Book();
					b.setId( rs.getInt("id") );
					b.setName( rs.getString("name") );
					b.setAuthor( rs.getString("author") );
					b.setSubject( rs.getString("subject") );
					b.setPrice( rs.getDouble("price") );
					list.add(b);
				}
			}
		}
		return list;
	}
	// get subjects
	public List<String> getSubjects() throws Exception {
		List<String> list = new ArrayList<>();
		String sql = "SELECT DISTINCT subject FROM BOOKS";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			try(ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					String subject = rs.getString("subject");
					list.add(subject);
				}
			}
		}
		return list;
	}

	// get book of given id
	public Book getBook(int id) throws Exception {
		Book b = null;
		String sql = "SELECT * FROM BOOKS WHERE id=?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, id);
			try(ResultSet rs = stmt.executeQuery()) {
				if(rs.next()) {
					b = new Book();
					b.setId( rs.getInt("id") );
					b.setName( rs.getString("name") );
					b.setAuthor( rs.getString("author") );
					b.setSubject( rs.getString("subject") );
					b.setPrice( rs.getDouble("price") );
				}
			}
		}
		return b;
	}
	// add book
	public int addBook(Book b) throws Exception {
		int cnt = -1;
		String sql = "INSERT INTO BOOKS VALUES(?, ?, ?, ?, ?)";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, b.getId());
			stmt.setString(2, b.getName());
			stmt.setString(3, b.getAuthor());
			stmt.setString(4, b.getSubject());
			stmt.setDouble(5, b.getPrice());
			cnt = stmt.executeUpdate();
		}
		return cnt;
	}
	// update book
	// UPDATE BOOKS SET name=?, author=?, subject=?, price=? WHERE id=?
	// delete book
	// DELETE FROM BOOKS WHERE id=?

	// update price
	public int updatePrice(int id, double price) throws Exception {
		int cnt = -1;
		String sql = "CALL SP_UPDATE_PRICE(?, ?)";
		try(CallableStatement stmt = con.prepareCall(sql)) {
			stmt.setInt(1, id);
			stmt.setDouble(2, price);
			boolean isResultSet = stmt.execute();
			if(!isResultSet)
				cnt = stmt.getUpdateCount();
		}
		return cnt;
	}
	
	// get price
	public double getPrice(int id) throws Exception {
		double price = 0.0;
		String sql = "CALL SP_GET_PRICE(?, ?)";
		try(CallableStatement stmt = con.prepareCall(sql)) {
			stmt.setInt(1, id);
			stmt.registerOutParameter(2, Types.DOUBLE);
			stmt.execute();
			price = stmt.getDouble(2);
		}
		return price;
	}
}






