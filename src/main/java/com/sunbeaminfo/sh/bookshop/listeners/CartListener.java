package com.sunbeaminfo.sh.bookshop.listeners;

import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class CartListener implements HttpSessionListener, ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("application started...");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("application stopped...");
	}
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("new session created.");
		HttpSession session = se.getSession();
		session.setAttribute("cart", new ArrayList<Integer>());
	}
	
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("session destroyed.");
	}
}
