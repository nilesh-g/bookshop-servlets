package com.sunbeaminfo.sh.bookshop.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sunbeaminfo.sh.bookshop.daos.BookDao;
import com.sunbeaminfo.sh.bookshop.entities.Book;

//@WebServlet("/showcart")
public class ShowCartServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// get bookids from cart and display them
		HttpSession session = req.getSession();
		List<Integer> cart = (List<Integer>) session.getAttribute("cart");

		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Cart</title>");
		out.println("</head>");
		String color = this.getServletContext().getInitParameter("color");
		out.printf("<body bgcolor='%s'>\r\n", color);


		Cookie[] cookies = req.getCookies();
		String uname = "";
		for(Cookie c: cookies) {
			if(c.getName().equals("uname"))
				uname = c.getValue();
		}
		out.printf("Hello, %s <hr/>\r\n", uname);

		double total = 0.0;
		try (BookDao dao = new BookDao()) {
			dao.open();
			for (int id : cart) {
				Book b = dao.getBook(id);
				total += b.getPrice();
				out.println(b.toString());
				out.println("<br/>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		out.printf("<br/>Total : %.2f\r\n", total);
		out.println("<br/><br/>");
		out.println("<a href='logout'>Sign Out</a>");
		out.println("</body>");
		out.println("</html>");
	}
}
