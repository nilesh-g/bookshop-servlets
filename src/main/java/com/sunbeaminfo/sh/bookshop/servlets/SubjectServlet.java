package com.sunbeaminfo.sh.bookshop.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunbeaminfo.sh.bookshop.daos.BookDao;

public class SubjectServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> subjects = new ArrayList<String>();
		try (BookDao dao = new BookDao()) {
			dao.open();
			subjects = dao.getSubjects();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Subjects</title>");
		out.println("</head>");
		String color = this.getServletContext().getInitParameter("color");
		out.printf("<body bgcolor='%s'>\r\n", color);

		
		Cookie[] cookies = req.getCookies();
		String uname = "";
		for(Cookie c: cookies) {
			if(c.getName().equals("uname"))
				uname = c.getValue();
		}
		out.printf("Hello, %s <hr/>\r\n", uname);
		
		out.println("<form method='get' action='books'>");
		for (String subject : subjects) 
			out.printf("<input type='radio' name='subject' value='%s'/> %s <br/>\r\n", 
					subject, subject);
		out.println("<input type='submit' value='Show Books'/>");
		out.println("</form>");
		
		out.println("<form method='get' action='showcart'>");
		out.println("<input type='submit' value='Show Cart'/>");
		out.println("</form>");
		
		String msg = (String) req.getAttribute("msg");
		if(msg != null)
			out.println(msg);
		out.println("</body>");
		out.println("</html>");

	}
}
