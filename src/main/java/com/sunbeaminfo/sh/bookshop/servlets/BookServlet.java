package com.sunbeaminfo.sh.bookshop.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunbeaminfo.sh.bookshop.daos.BookDao;
import com.sunbeaminfo.sh.bookshop.entities.Book;

public class BookServlet  extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String subject = req.getParameter("subject");
		List<Book> books = new ArrayList<>();
		try (BookDao dao = new BookDao()) {
			dao.open();
			books = dao.getBooks(subject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Books</title>");
		out.println("</head>");
		String color = this.getServletContext().getInitParameter("color");
		out.printf("<body bgcolor='%s'>\r\n", color);

		
		Cookie[] cookies = req.getCookies();
		String uname = "";
		for(Cookie c: cookies) {
			if(c.getName().equals("uname"))
				uname = c.getValue();
		}
		out.printf("Hello, %s <hr/>\r\n", uname);

		out.println("<form method='post' action='addcart'>");
		for (Book b: books) 
			out.printf("<input type='checkbox' name='book' value='%s'/> %s "
					+ "<a href='books?subject=%s&id=%d'>details</a><br/>\r\n", 
					b.getId(), b.getName(), subject, b.getId());
		out.println("<input type='submit' value='Add Cart'/>");
		out.println("</form>");
		
		String bookId = req.getParameter("id");
		if(bookId != null) {
			int id = Integer.parseInt(bookId);
			try (BookDao dao = new BookDao()) {
				dao.open();
				Book b = dao.getBook(id);
				out.println("<div>");
				out.println(b.toString());
				out.println("</div>");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		out.println("</body>");
		out.println("</html>");
	}

}
