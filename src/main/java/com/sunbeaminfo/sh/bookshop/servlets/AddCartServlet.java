package com.sunbeaminfo.sh.bookshop.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddCartServlet   extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// get cart from session and add bookids in the cart
		HttpSession session = req.getSession();
		List<Integer> cart = (List<Integer>) session.getAttribute("cart");
		
		String[] bookIds = req.getParameterValues("book");
		int newBooks = 0;
		if(bookIds != null) {
			newBooks = bookIds.length;
			for (String bookId : bookIds) {
				int id = Integer.parseInt(bookId);
				cart.add(id);
			}
		}
		req.setAttribute("msg", "New books added: " + newBooks);
		
		//RequestDispatcher rd = req.getRequestDispatcher("subjects");
		RequestDispatcher rd = this.getServletContext()
				.getRequestDispatcher("/subjects");
		rd.forward(req, resp);		
	}

}
