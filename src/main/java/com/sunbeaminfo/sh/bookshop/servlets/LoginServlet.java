package com.sunbeaminfo.sh.bookshop.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sunbeaminfo.sh.bookshop.daos.CustomerDao;
import com.sunbeaminfo.sh.bookshop.entities.Customer;

public class LoginServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		// authenticate user
		boolean success = false;
		Customer cust = null;
		try(CustomerDao dao = new CustomerDao()) {
			dao.open();
			cust = dao.getCustomer(email);
			if(cust != null && cust.getPassword().equals(password))
				success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(success) {
			// create cookie for username
			Cookie c = new Cookie("uname", cust.getName());
			// c.setMaxAge(3600); // persistent cookie
			resp.addCookie(c);
			
			// create empty cart & add to the session
			HttpSession session = req.getSession();
			
			// -- moved into listener code
			//List<Integer> cart = new ArrayList<Integer>();
			//session.setAttribute("cart", cart);
			
			session.setAttribute("cust", cust);
			
			// if success go to subject servlet
			resp.sendRedirect("subjects");
			//resp.sendRedirect(resp.encodeRedirectURL("subjects"));
		} else {
			// if failed show message.
			resp.setContentType("text/html");
			PrintWriter out = resp.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>BookShop</title>");
			out.println("</head>");
			
			String color = this.getServletContext().getInitParameter("color");
			out.printf("<body bgcolor='%s'>\r\n", color);
			out.println("Invalid email or password.<br/>");
			out.println("<a href='index.html'>Login again</a>");
			out.println("</body>");
			out.println("</html>");
		}
	}
}





